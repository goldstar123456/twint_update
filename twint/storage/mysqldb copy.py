import mysql.connector
import sys
import time
import hashlib

from datetime import datetime
# database ="twints"
def Conn(database):
    if database:
        print("[+] Inserting into Mysql Database: " + str(database))       
        conn = init(database)
        if isinstance(conn, str):
            print(str)
            sys.exit(1)
    else:
        conn = ""

    return conn

def init(db):
    try:
        conn = mysql.connector.connect(host="localhost",user="root", passwd="")
        cursor = conn.cursor()
        cursor.execute("CREATE DATABASE IF NOT EXISTS " + db + " CHARACTER SET utf8 COLLATE utf8_general_ci") 
        conn = mysql.connector.connect(host="localhost",user="root", passwd="", database=db)
        cursor = conn.cursor()
        table_users = """
            CREATE TABLE IF NOT EXISTS
                users(
                    `id` int(24) not null,
                    `id_str` VARCHAR(256) not null,
                    `name` VARCHAR(256),
                    `username` VARCHAR(256) not null,
                    `bio` VARCHAR(256),
                    `location` VARCHAR(256),
                    `url` VARCHAR(256),
                    `join_date` VARCHAR(256) not null,
                    `join_time` VARCHAR(256) not null,
                    `tweets` int(24),
                    `following` int(24),
                    `followers` int(24),
                    `likes` int(24),
                    `media` int(24),
                    `private` int(24) not null,
                    `verified` int(24) not null,
                    `profile_image_url` VARCHAR(256) not null,
                    `background_image` VARCHAR(256),
                    `hex_dig`  VARCHAR(256) not null,
                    `time_update` int(24) not null,
                    PRIMARY KEY (id, hex_dig)
                ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci;
            """
        cursor.execute(table_users)
        # print("table_user")
        table_tweets = """
            CREATE TABLE IF NOT EXISTS
                tweets (
                    id int(24) not null,
                    id_str VARCHAR(256) not null,
                    tweet VARCHAR(256) default '',
                    conversation_id VARCHAR(256) not null,
                    created_at int(24) not null,
                    date VARCHAR(256) not null,
                    time VARCHAR(256) not null,
                    timezone VARCHAR(256) not null,
                    place VARCHAR(256) default '',
                    replies_count int(24),
                    likes_count int(24),
                    retweets_count int(24),
                    user_id int(24) not null,
                    user_id_str VARCHAR(256) not null,
                    screen_name VARCHAR(256) not null,
                    name VARCHAR(256) default '',
                    link VARCHAR(256),
                    mentions VARCHAR(256),
                    hashtags VARCHAR(256),
                    cashtags VARCHAR(256),
                    urls VARCHAR(256),
                    photos VARCHAR(256),
                    quote_url VARCHAR(256),
                    video int(24),
                    geo VARCHAR(256),
                    near VARCHAR(256),
                    source VARCHAR(256),
                    time_update int(24) not null,
                    `translate` VARCHAR(256) default '',
                    trans_src VARCHAR(256) default '',
                    trans_dest VARCHAR(256) default '',
                    PRIMARY KEY (id)
                ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci;
        """
        cursor.execute(table_tweets)
        # print("table_tweets")

        table_retweets = """
            CREATE TABLE IF NOT EXISTS
                retweets(
                    user_id int(24) not null,
                    username VARCHAR(256) not null,
                    tweet_id int(24) not null,
                    retweet_id int(24) not null,
                    retweet_date int(24) not null,
                    PRIMARY KEY (user_id, tweet_id),
                    FOREIGN KEY (user_id) REFERENCES users(id),
                    FOREIGN KEY (tweet_id) REFERENCES tweets(id)
                ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci;
        """
        cursor.execute(table_retweets)
        # print("table_retweets")
        table_reply_to = """
            CREATE TABLE IF NOT EXISTS
                replies(
                    tweet_id int(24) not null,
                    user_id int(24) not null,
                    username VARCHAR(256) not null,
                    PRIMARY KEY (user_id, tweet_id),
                    FOREIGN KEY (tweet_id) REFERENCES tweets(id)
                ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci;
        """
        cursor.execute(table_reply_to)

        table_favorites =  """
            CREATE TABLE IF NOT EXISTS
                favorites(
                    user_id int(24) not null,
                    tweet_id int(24) not null,
                    PRIMARY KEY (user_id, tweet_id),
                    FOREIGN KEY (user_id) REFERENCES users(id),
                    FOREIGN KEY (tweet_id) REFERENCES tweets(id)
                ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci;
        """
        cursor.execute(table_favorites)

        table_followers = """
            CREATE TABLE IF NOT EXISTS
                followers (
                    id int(24) not null,
                    follower_id int(24) not null,
                    PRIMARY KEY (id, follower_id),
                    FOREIGN KEY(id) REFERENCES users(id),
                    FOREIGN KEY(follower_id) REFERENCES users(id)
                ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci;
        """
        cursor.execute(table_followers)

        table_following = """
            CREATE TABLE IF NOT EXISTS
                following (
                    id int(24) not null,
                    following_id int(24) not null,
                    PRIMARY KEY (id, following_id),
                    FOREIGN KEY(id) REFERENCES users(id),
                    FOREIGN KEY(following_id) REFERENCES users(id)
                ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci;
        """
        cursor.execute(table_following)

        table_followers_names = """
            CREATE TABLE IF NOT EXISTS
                followers_names (
                    user VARCHAR(256) not null,
                    time_update int(24) not null,
                    follower VARCHAR(256) not null,
                    PRIMARY KEY (user, follower)
                ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci;
        """
        cursor.execute(table_followers_names)

        table_following_names = """
            CREATE TABLE IF NOT EXISTS
                following_names (
                    user VARCHAR(256) not null,
                    time_update int(24) not null,
                    follows VARCHAR(256) not null,
                    PRIMARY KEY (user, follows)
                ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci;
        """
        cursor.execute(table_following_names)
        # print("followin name")

        return conn
    except Exception as e:
        return str(e)

def fTable(Followers):
    if Followers:
        table = "followers_names"
    else:
        table = "following_names"

    return table

def uTable(Followers):
    if Followers:
        table = "followers"
    else:
        table = "following"

    return table

def follow(conn, Username, Followers, User):
    try:
        time_ms = round(time.time()*1000)
        cursor = conn.cursor()
        entry = (User, time_ms, Username,)
        table = fTable(Followers)
        query = f"INSERT INTO {table} VALUES(%s,%s,%s)"
        cursor.execute(query, entry)
        conn.commit()
    except mysql.connector.IntegrityError:
        pass

def get_hash_id(conn, id):
    cursor = conn.cursor()
    cursor.execute('SELECT hex_dig FROM users WHERE id = %s LIMIT 1', (id,))
    resultset = cursor.fetchall()
    return resultset[0][0] if resultset else -1

def user(conn, config, User):
    try:
        time_ms = round(time.time()*1000)
        cursor = conn.cursor()
        user = [int(User.id), User.id, User.name, User.username, User.bio, User.location, User.url,User.join_date, User.join_time, User.tweets, User.following, User.followers, User.likes, User.media_count, User.is_private, User.is_verified, User.avatar, User.background_image]

        hex_dig = hashlib.sha256(','.join(str(v) for v in user).encode()).hexdigest()
        entry = tuple(user) + (hex_dig,time_ms,)
        old_hash = get_hash_id(conn, User.id)

        if old_hash == -1 or old_hash != hex_dig:
            query = f"INSERT INTO users VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
            cursor.execute(query, entry)
        else:
            pass

        if config.Followers or config.Following:
            table = uTable(config.Followers)
            query = f"INSERT INTO {table} VALUES(%s,%s)"
            cursor.execute(query, (config.User_id, int(User.id)))

        conn.commit()
    except mysql.connector.IntegrityError:
        pass

def tweets(conn, Tweet, config):
    try:
        time_ms = round(time.time()*1000)
        cursor = conn.cursor()
        entry = (Tweet.id,
                    Tweet.id_str,
                    Tweet.tweet,
                    Tweet.conversation_id,
                    Tweet.datetime,
                    Tweet.datestamp,
                    Tweet.timestamp,
                    Tweet.timezone,
                    Tweet.place,
                    Tweet.replies_count,
                    Tweet.likes_count,
                    Tweet.retweets_count,
                    Tweet.user_id,
                    Tweet.user_id_str,
                    Tweet.username,
                    Tweet.name,
                    Tweet.link,
                    ",".join(Tweet.mentions),
                    ",".join(Tweet.hashtags),
                    ",".join(Tweet.cashtags),
                    ",".join(Tweet.urls),
                    ",".join(Tweet.photos),
                    Tweet.quote_url,
                    Tweet.video,
                    Tweet.geo,
                    Tweet.near,
                    Tweet.source,
                    time_ms,
                    Tweet.translate,
                    Tweet.trans_src,
                    Tweet.trans_dest)
        cursor.execute('INSERT INTO tweets VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);', entry)

        if config.Favorites:
            query = 'INSERT INTO favorites VALUES(%s,%s)'
            cursor.execute(query, (config.User_id, Tweet.id))

        if Tweet.retweet:
            query = 'INSERT INTO retweets VALUES(%s,%s,%s,%s,%s)'
            _d = datetime.timestamp(datetime.strptime(Tweet.retweet_date, "%Y-%m-%d %H:%M:%S"))
            cursor.execute(query, (int(Tweet.user_rt_id), Tweet.user_rt, Tweet.id, int(Tweet.retweet_id), _d))
        
        if Tweet.reply_to:
            for reply in Tweet.reply_to:
                query = 'INSERT INTO replies VALUES(%s,%s,%s)'
                cursor.execute(query, (Tweet.id, int(reply['user_id']), reply['username']))

        conn.commit()
    except mysql.connector.IntegrityError:
        pass
